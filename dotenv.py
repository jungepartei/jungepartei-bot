import os
from helpers import nodeadspaces

def load_dotenv(dotenv_path=".env", verbose=False):
    try:
        with open(dotenv_path, mode="r") as f:
            linenum = 0
            for line in f.read().split("\n"):
                linenum += 1
                parseline(line, linenum, dotenv_path=dotenv_path, verbose=verbose)
    except FileNotFoundError:
        return False

def parseline(line, linenum, dotenv_path, verbose):
    if nodeadspaces(line) == "":
        return False
    # Remove "" and ''
    line = line.replace("'", "")
    line = line.replace('"', '')
    # Skip comments
    if line.startswith("#"):
        if verbose: print(f"{dotenv_path}: Skipped comment in line {linenum}")
        return False
    # Parse line
    linesplit = line.split("=", 1)
    if len(linesplit) != 2:
        if line.startswith("source ") or line.startswith(". "):
            # Get path of sourced dotenv
            if line.startswith("source "):
                repsign = "source"
            elif line.startswith(". "):
                repsign = "."
            tdotenv_path = line.replace(f"{repsign} ", "", 1)
            # Parse sourced dotenv
            if verbose: print(f'{dotenv_path}: Parsing sourced dotenv file "{tdotenv_path}" in line {linenum}')
            load_dotenv(dotenv_path=tdotenv_path, verbose=verbose)
            return False
        else:
            raise SyntaxError(f'{dotenv_path}: line {linenum}: Reached end of line while searching for "="')
    # Define variable in environment
    key = nodeadspaces(linesplit[0])
    value = nodeadspaces(linesplit[1])
    if verbose: print(f'Parsed "{dotenv_path}" (line {linenum}): "{key}" = "{value}"')
    os.environ[key] = value
    return True
