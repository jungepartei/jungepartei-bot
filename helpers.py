import discord


def firstchar_cap(string):
    newstring = ""
    capsed = False
    for letter in string:
        if not capsed:
            newstring += letter.capitalize()
            capsed = True
        else:
            newstring += letter
    return newstring


def get_premention(ctx, message):
    if isinstance(ctx.channel, discord.DMChannel):
        pre = ""
    else:
        pre = f"{ctx.author.mention}, "
    return firstchar_cap(f"{pre}{message}")


def better_wordsss(number: int):
    if number == 1:
        return ""
    else:
        return "s"


def nodeadspaces(string):
    try:
        # Remove leading spaces
        while string.startswith(" "):
            string = string[1:]
        # Remove trailing spaces
        while string[-1] == " ":
            string = string[:-1]
        return string
    except IndexError:
        return ""
