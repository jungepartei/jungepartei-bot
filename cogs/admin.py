import os
import time
from datetime import datetime

import discord
from discord.ext import commands, tasks
from pytimeparse.timeparse import timeparse

from checks import checks
from config import read_config, write_config
from loghelper import log


class Admin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.check_ban_expiration.start()

    @commands.command()
    @commands.has_role(read_config("env", "ADMIN_ROLE"))
    async def botinfo(self, ctx, text):
        """
        Setzt den 'Spiel'-Status des Bots
        """
        await self.bot.change_presence(activity=discord.Game(text))
        await ctx.send("Botinfo changed!")
        await log(self.bot, title="Botinfo geändert", user=ctx.author, fields=[{"title": "Text", "content": text}],
                  color=discord.Color.green())

    @commands.guild_only()
    @commands.command()
    @commands.has_role(read_config("env", "ADMIN_ROLE"))
    async def joined(self, ctx, *, member: discord.Member):
        """
        Zeigt an wann jemand dem Server beigetreten ist
        """
        await ctx.send(
            f"`{member.display_name}` ist beigetreten am {member.joined_at.strftime('%d.%m.%Y um %H:%M:%S')}")

    @commands.guild_only()
    @commands.command()
    @commands.has_role(read_config("env", "ADMIN_ROLE"))
    async def kick(self, ctx, member: discord.Member, *reason):
        """
        Kickt einen Benutzer
        """
        await ctx.guild.kick(member)
        await ctx.send(f"`{member.display_name}` wurde gekickt. Er/Sie kann jederzeit wieder beitreten")
        reasontxt = ""
        if len(reason) == 0:
            reasontxt = "[Kein Grund]"
        for i in reason:
            reasontxt = reasontxt + i + " "
        await log(self.bot, title="Benutzer gekickt", user=ctx.author,
                  fields=[{"title": "Benutzer", "content": member.mention}, {"title": "Grund", "content": reasontxt}],
                  color=discord.Color.red())

    @commands.guild_only()
    @commands.command()
    @commands.has_role(read_config("env", "ADMIN_ROLE"))
    async def ban(self, ctx, member: discord.Member, *args):
        """
        Bannt einen Benutzer
        """
        reason = ""
        if len(args) == 0:
            reason = "[Kein Grund]"
        for i in args:
            reason = reason + i + " "
        try:
            await member.send(f"Du wurdest von FFF Germany gebannt. Angegebener Grund: {reason}\n"
                              "Wenn du das Gefühl hast, das dieser Bann nicht gerechtfertigt war, kannst du einen"
                              "Entbannungsantrag erstellen. Klicke dazu auf diesen Link und fülle das Formular aus:\n"
                              "https://pibot.eu/index.php/entbannungsantrag-discord-fff-germany/")
        except:
            await ctx.send("Ban-Nachricht konnte nicht zugestellt werden")
        await ctx.guild.ban(member)
        await ctx.send(
            f"{member.display_name} wurde permanent gebannt. Um den Ban aufzuheben gehe in die Servereinstellungen")
        await log(self.bot, title="Benutzer permanent gebannt", user=ctx.author,
                  fields=[{"title": "Benutzer", "content": member.mention}, {"title": "Grund", "content": reason}],
                  color=discord.Color.red())

    def _write_tempban(self, userid, lifttime):
        with open("config/config/bans.banfile", "a") as file:
            file.write(f'{userid}:{lifttime}\n')

    @commands.guild_only()
    @commands.command()
    @commands.has_role(read_config("env", "ADMIN_ROLE"))
    async def tempban(self, ctx, member: discord.Member, duration, *args):
        """
        Bannt einen Benutzer temporär
        """
        if not timeparse(duration):
            await ctx.send("Ungültige Zeit. Beispiel: 5d")
            return
        if member.bot:
            await ctx.send("Du kannst Bots nicht temporär bannen.")
            return
        lifttime = time.time() + timeparse(duration)
        self._write_tempban(member.id, lifttime)
        reason = ""
        if len(args) == 0:
            reason = "[Kein Grund]"
        for i in args:
            reason = reason + i + " "
        try:

            await member.send(f"Du wurdest von FFF Germany temporär gebannt. Angegebener Grund: {reason}\n"
                              "Wenn du das Gefühl hast, dass dieser Bann nicht gerechtfertigt war oder du nach "
                              "der angegeben Zeit nicht wieder entbannt wurdest, kannst du einen"
                              "Entbannungsantrag erstellen. Klicke dazu auf diesen Link und fülle das Formular aus:\n"
                              "https://pibot.eu/index.php/entbannungsantrag-discord-fff-germany/")
        except:
            await ctx.send("Ban-Nachricht konnte nicht zugestellt werden")

        timestampStr = datetime.fromtimestamp(lifttime).strftime('%d. %b %Y, %H:%M:%S Uhr')
        await ctx.send(
            f"{member.display_name} wurde temporär bis zum {timestampStr} gebannt."
            f" Um den Ban manuell aufzuheben gehe in die Servereinstellungen")

        await log(self.bot, title="Benutzer temporär gebannt", user=ctx.author,
                  fields=[{"title": "Benutzer", "content": member.mention}, {"title": "Grund", "content": reason},
                          {"title": "Dauer", "content": duration}], color=discord.Color.red())
        await ctx.guild.ban(member)

    @tasks.loop(seconds=10.0)
    async def check_ban_expiration(self):
        """
        Check if a ban expired and lift it then.
        """
        print("Checking for outdated bans...")
        guild = self.bot.get_guild(int(read_config("env", "GUILD_ID")))
        with open("config/config/bans.banfile", "r") as file:
            lines = file.readlines()
        for i in lines:
            if datetime.now() > datetime.fromtimestamp(float(i.split(":")[1])):
                user = await self.bot.fetch_user(int(i.split(":")[0]))
                await guild.unban(user)
                await log(self.bot, title="Temporärer Ban aufgehoben", user=self.bot.user,
                          fields=[{"title": "Benutzer", "content": user.mention}], color=discord.Color.dark_green())
                lines.remove(i)
        os.remove("config/config/bans.banfile")
        with open("config/config/bans.banfile", "w") as file:
            for line in lines:
                file.write(line)

    @commands.command()
    @commands.guild_only()
    @commands.check(checks.check_if_is_main_server)
    @commands.has_role(read_config("env", "ADMIN_ROLE"))
    async def raid(self, ctx, mode):
        """
        Gibt der RAID Rolle die Berechtigung zum kicken
        """
        raid_role = ctx.guild.get_role(int(read_config("env", "RAID_ROLE")))
        permissions = raid_role.permissions
        if mode == "on":
            permissions.kick_members = True
            await raid_role.edit(permissions=permissions)
            await ctx.send("RAID Modus wurde eingeschaltet. Alle Benutzer mit der RAID Rolle können jetzt kicken.")

        elif mode == "off":
            permissions.kick_members = False
            await raid_role.edit(permissions=permissions)
            await ctx.send("RAID Modus wurde ausgeschaltet. Die Benutzer mit der RAID Rolle können nicht mehr kicken.")
        else:
            ctx.send("Benutzung: `!admin raid on` oder `!admin raid off`")

        await log(self.bot, title="RAID Modus geändert", user=ctx.author,
                  fields=[{"title": "Neue Einstellung", "content": mode}], color=discord.Color.red())


def setup(bot):
    bot.add_cog(Admin(bot))
