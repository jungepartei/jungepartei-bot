import discord
from discord.ext import commands
from helpers import get_premention
from traceback import format_exception


def setup(bot):
    @bot.event
    async def on_command_error(ctx, error):
        err = getattr(error, "original", error)

        if isinstance(err, commands.CommandNotFound):
            await ctx.send(get_premention(ctx, "dieses Kommando existiert nicht!"))
        elif isinstance(err, commands.CommandOnCooldown):
            await ctx.send(
                'Dieses Kommando hat einen Cooldown. Versuche es nochmal in {:.2f} Sekunden!'.format(error.retry_after))
        elif isinstance(err, commands.NonMainServer):
            await ctx.send(get_premention(ctx, "du kannst dieses Kommando nur auf dem FFF Hauptserver verwenden!"),
                           delete_after=5)
        elif isinstance(err, commands.NoPrivateMessage):
            await ctx.send(get_premention(ctx, "du kannst diese Kommando nicht in DMs verwenden!"))
        elif isinstance(err, commands.errors.MissingRequiredArgument):
            await ctx.send(get_premention(ctx, f'das Argument "{err.args[0].split(" ")[0]}" fehlt!'))
        elif isinstance(err, commands.errors.MissingPermissions):
            await ctx.send(get_premention(ctx, f'dir Fehlen folgende Berechtigungen: {", ".join(err.missing_perms)}'),
                           delete_after=5)
        elif isinstance(err, commands.errors.MissingRole):
            await ctx.send(
                get_premention(ctx, f'um dieses Kommando zu benutzen, muss du in der Role "{err.missing_role}" sein!'))
        elif isinstance(err, commands.errors.BadArgument):
            await ctx.send(get_premention(ctx,
                                          f'das Argument {err.args[0].split(" ")[-1][:-1]} muss ein {err.args[0].split(" ")[2]} sein!'))
        else:
            await ctx.send("Ein Fehler ist aufgegtreten:")
            await ctx.send(f'```{"".join(format_exception(type(err), err, err.__traceback__, 4))}```')
            raise error
