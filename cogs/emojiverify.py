from config import read_config
from discord.ext import commands
import discord
from checks import checks
from loghelper import log
from time import sleep


class EmojiVerify(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.guild_only()
    @commands.check(checks.check_if_is_main_server)
    async def updatereactions(self, ctx):
        for message in read_config("roles.json", "messages"):
            guild = self.bot.get_guild(int(read_config("env", "GUILD_ID")))
            channel = self.bot.get_channel(message["channel_id"])
            message2 = await channel.fetch_message(message["id"])
            reactions = message2.reactions
            for reaction in reactions:
                await reaction.remove(self.bot.user)
            print("next msg")
            print(message2.id)
            for emoji in message["emojis"]:
                emoji2 = discord.utils.get(guild.emojis, name=emoji)
                try:
                    await message2.add_reaction(emoji2)
                except:
                    await message2.add_reaction(emoji)

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, added_reaction):
        for message in read_config("roles.json", "messages"):
            if added_reaction.message_id == message["id"]:
                guild = self.bot.get_guild(int(read_config("env", "GUILD_ID")))
                member = guild.get_member(added_reaction.user_id)
                user = self.bot.get_user(added_reaction.user_id)
                channel = self.bot.get_channel(added_reaction.channel_id)
                current_message = await channel.fetch_message(added_reaction.message_id)
                print(self.bot.user.id)
                print(added_reaction.user_id)
                if added_reaction.user_id == self.bot.user.id:
                    print("IS BOT")
                    return
                if message["multiselect"] is False:
                    for current_reaction_of_message in current_message.reactions:

                        print("New reaction:", added_reaction.emoji)
                        print("Checking against:", current_reaction_of_message.emoji)
                        if str(current_reaction_of_message.emoji) != str(added_reaction.emoji):
                            await current_reaction_of_message.remove(user)
                    for emoji, roles in message["emojis"].items():
                        for role in roles:
                            await member.remove_roles(guild.get_role(role))
                roles_to_add = message["emojis"][str(added_reaction.emoji)]
                for role_id in roles_to_add:
                    await member.add_roles(
                        guild.get_role(role_id))

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, reaction):
        for message in read_config("roles.json", "messages"):
            if reaction.message_id == message["id"]:
                guild = self.bot.get_guild(int(read_config("env", "GUILD_ID")))
                member = guild.get_member(reaction.user_id)
                user = self.bot.get_user(reaction.user_id)
                channel = self.bot.get_channel(reaction.channel_id)
                current_message = await channel.fetch_message(reaction.message_id)
                if current_message.author.bot:
                    return
                roles_to_remove = message["emojis"][str(reaction.emoji)]
                for role_id in roles_to_remove:
                    await member.remove_roles(
                        guild.get_role(role_id))


def setup(bot):
    # @bot.event
    # async def on_ready():
    #     print("Hi")
    #     # THIS IS NOT WORKING YET
    #     channel = bot.get_channel(706955862043787314)
    #     await channel.send("ddsad")
    #     for message in read_config("roles.json", "messages"):
    #         channel = bot.get_channel(message["channel_id"])
    #         message2 = await channel.fetch_message(message["id"])
    #         for emoji in message["emojis"]:
    #             await message2.add_reaction(emoji)


    bot.add_cog(EmojiVerify(bot))
