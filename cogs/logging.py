from loghelper import log
from discord import Color


def setup(bot):
    @bot.event
    async def on_member_join(member):
        channel = await member.create_dm()
        await channel.send("Willkommen auf dem Server der Jungen Partei! :partying_face: \n"
                        "Du möchtest bei der Jungen Partei mitmachen? Schau doch mal in #mitmachen vorbei.\n"
                        "Bitte ließ dir die #regeln durch und richtige dir dein Konto in #irgendwas ein.\n"
                        "Wichtige Links der Partei findest du unter #links.\n\n"
                        "Viel Spaß auf dem Server!")
        await log(bot, fields=[{"title": "Benutzer", "content": member.mention}],
                  title="Benutzer ist dem Server beigetreten", user=member, color=Color.green())

    @bot.event
    async def on_member_remove(member):
        await log(bot, fields=[{"title": "Benutzer", "content": member.mention}],
                  title="Benutzer hat den Server verlassen", user=member, color=Color.red())

    @bot.event
    async def on_message_edit(before, after):
        await log(bot, fields=[{"title": "Alte Nachricht", "content": before.content},
                               {"title": "Bearbeite Nachricht", "content": after.content},
                               {"title": "Channel", "content": f"#{after.channel}"},
                               {"title": "ID", "content": after.id}],
                  title="Nachricht bearbeitet", user=before.author, color=Color.blue())

    @bot.event
    async def on_message_delete(message):
        await log(bot, fields=[{"title": "Nachricht", "content": message.content},
                               {"title": "ID", "content": message.id},
                               {"title": "Channel", "content": f"#{message.channel}"}
                               ],
                  title="Nachricht gelöscht", user=message.author, color=Color.dark_blue())

    @bot.event
    async def on_member_unban(guild, user):
        await log(bot, fields=[{"title": "Benutzer", "content": user.mention}],
                  title="Benutzer entbannt", user=user, color=Color.dark_green())

    @bot.event
    async def on_guild_role_create(role):
        await log(bot, fields=[{"title": "Rolle", "content": role.mention}],
                  title="Rolle erstellt", user=bot.user, color=Color.orange())

    @bot.event
    async def on_guild_role_delete(role):
        await log(bot, fields=[{"title": "Rolle", "content": role.name}],
                  title="Rolle gelöscht", user=bot.user, color=Color.dark_orange())

    @bot.event
    async def on_member_update(before, after):
        if before.nick != after.nick:
            await log(bot, fields=[{"title": "Alter Nickname", "content": before.nick},
                                   {"title": "Neuer Nickname", "content": after.nick}], title="Nickname geändert",
                      user=after, color=Color.magenta())

    @bot.event
    async def on_user_update(before, after):
        if before.name != after.name:
            await log(bot, fields=[{"title": "Alter Name", "content": before.name},
                                   {"title": "Neuer Name", "content": after.name}], title="Benutzername geändert",
                      user=after, color=Color.magenta())
        if before.avatar != after.avatar:
            await log(bot, fields=[{"title": "Altes Bild", "content": before.avatar_url},
                                   {"title": "Neues Bild", "content": after.avatar_url}], title="Avatar geändert",
                      user=after, color=Color.gold())
