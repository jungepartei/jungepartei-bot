import discord
from discord.ext import commands


class JungePartei(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def links(self, ctx):
        """
        Sendet den Link zu diesem Server
        """
        async with ctx.channel.typing():
            await ctx.send("**Website: **https://jungepartei.de/\n"
                           "**CodiMD: **https://md.jungepartei.de/s/Willkommen\n"
                           "**Rocket.chat: **https://chat.jungepartei.de/")


def setup(bot):
    bot.add_cog(JungePartei(bot))
