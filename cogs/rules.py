import discord
from discord.ext import commands
from config import read_config, write_config
from discord.ext import commands
import discord
from checks import checks
from loghelper import log


class Rules(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.guild = self.bot.get_guild(int(read_config("env", "GUILD_ID")))

    @commands.check(checks.check_if_is_main_server)
    @commands.has_role(read_config("env", "ADMIN_ROLE"))
    @commands.group()
    async def rule(self, ctx):
        """
        Fügt eine Regel hinzu.
        """
        pass

    @rule.command()
    async def add(self, ctx, header, message):
        rules = read_config("rules.json", "rules")
        rules.append({"message": message, "header": header})

        write_config("rules.json", "rules", rules)
        await ctx.send(
            "Die Regel wurde hinzugefügt. Sende `!rule update` in den Regelchannel um die Regeln neuzuladen.")
        await log(self.bot, title="Regel hinzugefügt", user=ctx.author,
                  fields=[{"title": "Header", "content": header}, {"title": "Content", "content": message}])

    @rule.command()
    async def insert(self, ctx, position: int, header, message):
        rules = read_config("rules.json", "rules")
        rules.insert(position, {"message": message, "header": header})

        write_config("rules.json", "rules", rules)
        await ctx.send(
            "Die Regel wurde eingefügt. Sende `!rule update` in den Regelchannel um die Regeln neuzuladen.")
        await log(self.bot, title="Regel eingefügt", user=ctx.author,
                  fields=[{"title": "Header", "content": header},
                          {"title": "Content", "content": message},
                          {"title": "At ID", "content": position}])

    @rule.command()
    async def overwrite(self, ctx, rule_id: int, header, message):
        rules = read_config("rules.json", "rules")
        try:
            rules[rule_id] = {"message": message, "header": header}
            write_config("rules.json", "rules", rules)
            await ctx.send(
                "Die Regel wurde überschrieben. Sende `!rule update` in den Regelchannel um die Regeln neuzuladen.")
            await log(self.bot, title="Regel überschrieben", user=ctx.author,
                      fields=[{"title": "Header", "content": header}, {"title": "Content", "content": message}])
        except IndexError:
            await ctx.send("Diese Regel existiert nicht! Der Index fängt bei 0 an.")

    @rule.command()
    async def remove(self, ctx, rule_id: int):
        rules = read_config("rules.json", "rules")
        try:
            await log(self.bot, title="Regel eingefügt", user=ctx.author,
                      fields=[{"title": "ID", "content": rule_id},
                              {"title": "Header", "content": rules[rule_id]["header"]},
                              {"title": "Content", "content": rules[rule_id]["message"]}
                              ])

            del rules[rule_id]
            write_config("rules.json", "rules", rules)
            await ctx.send(
                "Die Regel wurde gelöscht. Sende `!rule update` in den Regelchannel um die Regeln neuzuladen.")
        except IndexError:
            await ctx.send("Diese Regel existiert nicht! Der Index fängt bei 0 an.")

    @rule.command()
    async def clear(self, ctx):

        await log(self.bot, title="Alle Regeln gelöscht", user=ctx.author,
                  fields=[])

        rules = []
        write_config("rules.json", "rules", rules)
        await ctx.send(
            "Alle Regeln gelöscht. Sende `!rule update` in den Regelchannel um die Regeln neuzuladen.")

    @rule.command()
    async def list(self, ctx):
        rules = read_config("rules.json", "rules")
        embed = discord.Embed(title="Liste der Regeln")
        for idx, rule in enumerate(rules):
            embed.add_field(name=f'{rule["header"]} - ID: {idx}', value=rule['message'], inline=False)
        await ctx.send("", embed=embed)

    @rule.command()
    async def update(self, ctx):
        rules = read_config("rules.json", "rules")
        await ctx.channel.purge()
        for rule in rules:
            embed = discord.Embed(title=rule['header'], description=rule["message"])
            await ctx.send("", embed=embed)
        await log(self.bot, title="Regeln wurden aktualisiert", user=ctx.author,
                  fields=[],
                  color=discord.Color.green())
        message = await ctx.send("Wenn du die Regeln akzepierst, drücke das Emoji unter dieser Nachricht:")
        await message.add_reaction("✅")  # Name is not empty, it contains an emoji
        write_config("rules.json", "rules", rules)

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, event):
        user = self.bot.get_user(event.user_id)
        member = self.guild.get_member(event.user_id)
        channel = self.bot.get_channel(event.channel_id)
        reaction = event.emoji
        if str(reaction) == "✅":
            await member.add_roles(
                self.guild.get_role(int(read_config("env", "VERIFIED_ROLE_ID"))))


def setup(bot):
    bot.add_cog(Rules(bot))
