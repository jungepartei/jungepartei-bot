import os
import time
from datetime import datetime

import discord
from discord.ext import commands, tasks
from pytimeparse.timeparse import timeparse

from checks import checks
from config import read_config, write_config
from loghelper import log
import requests


class Admin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.before_song = ""
        self.check_song_change.start()

    @tasks.loop(seconds=10.0)
    async def check_song_change(self):
        """
        Check if a ban expired and lift it then.
        """
        result = requests.get("https://api.laut.fm/station/jungeparteifm/current_song").json()
        if self.before_song != result["id"]:
            embed = discord.Embed(title="Now Playing", color=discord.Color.blurple())
            timestamp = datetime.now()
            timestamp = timestamp.replace(hour=timestamp.hour - 2)
            embed.timestamp = timestamp
            embed.add_field(name="Titel", value=result["title"])
            embed.add_field(name="Von", value=result["artist"]["name"])
            embed.set_footer(text=f'Junges Radio',
                             icon_url="https://cdn.pixabay.com/photo/2018/01/30/13/29/record-3118786_960_720.png")
            await self.bot.get_channel(int(read_config("env", "LAUTFM_CHANNEL_ID"))).send("", embed=embed)
        self.before_song = result["id"]


def setup(bot):
    bot.add_cog(Admin(bot))
