from discord.ext import commands
import discord


class MyHelpCommand(commands.MinimalHelpCommand):
    def __init__(self, **options):
        super().__init__(**options)
        self.commands_heading = "Befehle:"
        self.no_category = "Sonstiges:"
        self.command_attrs = {"name": "help", "aliases": ["hilfe"]}

    def get_opening_note(self):
        """Returns help command's opening note. Overriding to have it in german.
        """
        command_name = self.invoked_with
        return "Benutze `{0}{1} [command]` für genauere Hilfe zu einem Befehl.\n" \
               "Du kannst auch `{0}{1} [category]` benutzen um Hilfe für eine Kategorie zu bekommen.".format(
            self.clean_prefix,
            command_name)

    def get_ending_note(self):
        """Returns help command's ending note. This is mainly useful to override for i18n purposes."""
        return "Viel Spaß mit dem Bot!.\n" \
               "Programmiert von: Niwla23 und tuxifan"

    def get_command_signature(self, command):
        return '{0.clean_prefix}{1.qualified_name} {1.signature}'.format(self, command)


class Help(commands.Cog):
    def __init__(self, bot):
        self._original_help_command = bot.help_command
        bot.help_command = MyHelpCommand()
        bot.help_command.cog = self
        self.bot = bot
        with open("config/VERSION", "r") as file:
            self.VERSION = file.read()

    def cog_unload(self):
        self.bot.help_command = self._original_help_command

    @commands.command()
    async def info(self, ctx):
        """
        Informationen zu diesem Bot
        """

        def get_contributors():
            contributors = [{"name": "Niwla23", "id": 533383451676639252},
                            {"name": "Tuxifan", "id": 609486822715818000}]
            text = ""
            for contributor in contributors:
                try:
                    text = text + self.bot.get_user(contributor["id"]).mention + ", "
                except AttributeError:
                    text = text + contributor["name"] + ", "
            return text[:-2]

        async with ctx.channel.typing():
            embed = discord.Embed(title="Informationen")
            embed.add_field(name="Source Code", value="https://gitlab.com/jungepartei/jungepartei-bot", inline=False)
            embed.add_field(name="Version", value=self.VERSION, inline=False)
            embed.add_field(name="Contributors", value=f"{get_contributors()}", inline=False)
            embed.add_field(name="Lizenz",
                            value="Dieser Bot ist freie Software. Es wird keine Garantie oder Haftung übernommen. "
                                  "Der Quellcode ist unter MIT lizensiert. Der laufende Bot wird von GitLab gebaut, "
                                  "deshalb läuft nie neuerer Code in Production als auf GitLab gepusht wurde.",
                            inline=False)
            embed.add_field(name="Datenschutz",
                            value="Wir speichern Bearbeitungen von Nachrichten, Löschungen,"
                                  " Änderungen am Nutzerprofil, Beitritte, Verlassen des Servers sowie"
                                  " Änderungen der Rollen in einem Log Channel.\n"
                                  "Im Falle eines temporären Bans werden die Userid sowie der zeitpunkt des unbans auf"
                                  "einem Server mit Standort in Deutschland gespeichert. Diese Daten werden gelöscht, "
                                  "nachdem der Ban aufgehoben wurde. Bei Fragen zur Datenspeicherung wende dich bitte"
                                  "an einen der Admins oder an Niwla23.",
                            inline=False)
            await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Help(bot))
