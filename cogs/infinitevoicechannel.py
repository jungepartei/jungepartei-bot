import discord
from discord.ext import commands, tasks


class InfiniteVoiceChannel(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.nums_used = [1]
        self.current_channel = 1

    def get_free_num(self):
        biggest = max(self.nums_used)
        for number in range(1, biggest):
            if number not in self.nums_used:
                return number
        return biggest + 1

    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        if after.channel.name.startswith("🎤┊Talk"):
            self.current_channel += 1
            new_channel_num = self.get_free_num()
            self.nums_used.append(new_channel_num)
            await member.guild.create_voice_channel(f"🎤┊Talk {new_channel_num}",
                                                    category=discord.utils.get(member.guild.categories, name='Talk'))
            for channel in member.guild.voice_channels:
                if channel.name.startswith(
                        "🎤┊Talk") and channel.name != "🎤┊Talk" and channel.name != f"🎤┊Talk {new_channel_num}":
                    if len(channel.members) == 0:
                        print(channel.name.split(" ")[1])
                        try:
                            self.nums_used.remove(int(channel.name.split(" ")[1]))
                        except:
                            pass
                        print(self.nums_used)
                        await channel.delete()


def setup(bot):
    bot.add_cog(InfiniteVoiceChannel(bot))
